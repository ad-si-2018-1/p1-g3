const time = require('../Model/time').time;
exports.global = {

    //Server Name
    'serverName': 'localhost',

    //Mantenhe Os clientes
    'clients': [],

    // Lista de Canais
    'channels': [],

    //Allow Prefixes to Channels
    'channelNamesPrefixes': ['#', '&', '!', '+'],

    //Maximum Channel Name Size
    'maximumChannelNameSize': 50,

    'pingInterval': 3,

    //Messages
    'messages': [],

    'pingMessage': 'PING localhost ?\r\n',

    //Commands counter
    'counter': {},

    //Operator password for shifting to operator mode with the OPER command *******************
    //'operatorpassword': "senhadeoperador",

    //Modos Disponiveis
    'userModes': {
        'o': 'operator',
        'a': 'away',
        'i': 'invisible',
        'w': 'wallop reciever',
        'r': 'restricted user connection',
        '0': 'local operator',
    },

    //Modelo para criar o serviço
    'service': function(distribution, type, info) {
        this.distribution = distribution;
        this.type = type;
        this.info = info;
    },

    //Mensagem do Dia
    'motd': '*MENSAGEM MOTIVACIONAL DO DIA PARA TE FAZER MAIS FELIZ',

    //Nodejs Version
    'version': 'v8.4.0',

    //Hora
    'hms': time,

	//Momento em que o server foi iniciado
	'started': Date.now()
}