exports.Channel = class {
    constructor(name) {
        this.name = name;
        this.creationDate = Date.now();
        this.members = [];
        this.topic = null;
        this.mode = "=";
        this.operator = null;
        //this.messages = [];
    }
}