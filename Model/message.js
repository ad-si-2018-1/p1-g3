exports.Message = class {
    constructor(sender, receiver, text) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.channelName = null;
        this.errorMessage = null;
        this.dateTime = Date.now();
    }
}