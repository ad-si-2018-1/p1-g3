exports.erroMessages = {
    'ERR_NOSUCHNICK': [401, '<nickname> :No such nick/channel\r\n'],
    'ERR_NOSUCHCHANNEL': [403, '<channel name> :No such channel\r\n'],
    'ERR_CANNOTSENDTOCHAN': [404, '<channel> :Cannot send to channel\r\n'],
    'ERR_NORECIPIENT': [411, ' :No recipient given <command>\r\n'],
    'ERR_NOTEXTTOSEND': [412, ' :No text to send\r\n'],
    'ERR_USERNOTINCHANNEL': [441, '<nick> <channel> :They are not on that channel\r\n'],
    'ERR_NOTONCHANNEL': [442, '<channel> :You are not on that channel\r\n'],
    'ERR_USERONCHANNEL': [443, '<user> <channel> :is already on channel\r\n'],
    'ERR_NEEDMOREPARAMS': [461, '<command> :Not enough parameters\r\n'],
    'ERR_BADCHANNELKEY': [475, '<channel> :Cannot join channel (+k)\r\n'],
    'ERR_CHANOPRIVSNEEDED': [482, '<channel> :You are not channel operator\r\n']

}