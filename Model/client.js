exports.Client = class {

    constructor(socket) {
        this.socket = socket;
        this.nick = "";
        this.realName = "";
        this.mode = [];
        this.channels = [];
        this.hashedPassword = "";
        this.pass = "";
        this.pingInformation = {
            "time": Date.now(),
            /*
                1-Active
                2-Ping send
            */
            "status": 1
        }

    }
}