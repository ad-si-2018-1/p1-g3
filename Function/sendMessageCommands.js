var Message = require('../Model/Message').Message;
var errorMessages = require('../Model/errorMessages').erroMessages;

exports.privmsg = function(args, client, global) {
    genericSender(args, client, global);
}

exports.notice = function(args, client, global) {
    genericSender(args, client, global);
}


//Support Functions
function genericSender(args, client, global) {

    if (args.length < 2) {
        var error = errorMessages['ERR_NORECIPIENT'][1].replace('<command>', args[0]);
        client.socket.write(error);
        return;
    }

    if (!args.join(" ").includes(":")) {
        message = new Message(client.name, null, null);

        message.errorMessage = errorMessages['ERR_NOTEXTTOSEND'][1];

        sendMessage(client.socket, message);
        global.messages.push(message);

        return;
    }

    var receiverName = args[1].toUpperCase();
    var message = args[2].toString();
    var receiver = null;

    if (global.channelNamesPrefixes.includes(receiverName.charAt(0))) {
        receiver = global.channels.find(channel => channel.name == receiverName);

        sendMessageChannel(client, receiver, args, global);

    } else if ((message.charAt(0) == ":")) {

        receiver = global.clients.find(client => client.nick == receiverName);
        sendMessageToUser(client, receiver, args, global);
    }

    return;

}

function makeMessage(sender, args, global) {
    var commandName = args[0].toUpperCase();
    var receiver = args[1].toUpperCase();
    var message = ":" + args.join(" ").split(":")[1];
    message = ":" + sender.nick + "!" + sender.realName + "@" + global.serverName + " " + commandName + " " + receiver + " " + message + "\r\n";

    return message;
}

function sendMessageChannel(sender, receiver, args, global) {

    if (receiver != null) {

        var membersNicks = receiver.members;
        var receiversNames = getReceiversHostNames(membersNicks, sender.nick, global);
        var textMessage = makeMessage(sender, args, global);
        var message = new Message(sender.socket.name, receiversNames, textMessage);
        message.channelName = receiver.name;

        if (!membersNicks.includes(sender.nick.toUpperCase())) {
            message.errorMessage = errorMessages['ERR_NOTONCHANNEL'][1].replace('<channel>', receiver.name);
            sendMessage(sender.socket, message);

        } else if (membersNicks.length == 1) {
            message.errorMessage = receiver.name.toUpperCase() + " :You're the only one on the channel.\r\n";
            sendMessage(sender.socket, message);

        } else {
            var clientMembers = global.clients.filter(client => membersNicks.includes(client.nick.toUpperCase()) && client.socket.name !== sender.socket.name);

            clientMembers.forEach(function(client) {
                sendMessage(client.socket, message);
            });

        }

        global.messages.push(message);

    } else {
        var textMessage = ":" + args.join(" ").split(":")[1];
        var message = new Message(sender.socket.name, args[1], textMessage);
        message.errorMessage = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', args[1].toUpperCase());

        sendMessage(sender.socket, message);
        global.messages.push(message);
    }
}

function getReceiversHostNames(membersNicks, senderNick, global) {
    var hotsNames = [];

    membersNicks.forEach(function(nick) {
        var client = global.clients.find(client => client.nick.toUpperCase() == nick && client.nick != senderNick);
        if (client != null)
            hotsNames.push(client.socket.name);

    });

    return hotsNames;
}

function sendMessageToUser(sender, receiver, args, global) {


    if (receiver != null) {
        var textMessage = makeMessage(sender, args, global);
        var message = new Message(sender.socket.name, receiver.socket.name, textMessage);
        sendMessage(receiver.socket, message);

    } else {
        var textMessage = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', args[1].toUpperCase());
        var message = new Message(sender.socket.name, receiver, textMessage);
        sendMessage(sender.socket, message, global);
    }

}

function sendMessage(socket, message) {

    socket.write(message.errorMessage === null ? message.text : message.errorMessage);

    return;
}