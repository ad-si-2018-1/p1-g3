exports.ping = function(args, client, global) {
    var pong = args[1];
    client.socket.write("Comando PING: ");
    //Verifica Se Parametro é válido
    if (!pong) {
        client.socket.write("PING :Erro de sintaxe.\r\n");
        //Verifica Se são os do server
    }

    if (pong === "127.0.0.1" || pong.toUpperCase() === "LOCALHOST") {
        //Pong para o Cliente
        client.socket.write("PONG " + global.serverName + " " + client.nick + ".\r\n");
    } else {
        client.socket.write("PING :Server não identificado.\r\n");
    }

    return;

}
exports.pong = function(args, client, global) {
    if (args.length < 2 || args.length > 3) {
        client.socket.write("PONG :Erro de sintaxe.\r\n");
        return;
    }

    if (args[1] == global.serverName || args[1] == "127.0.0.1")
        updatePingInformations(client);
}


function updatePingInformations(client) {
    client.pingInformation.time = Date.now();
    client.pingInformation.status = 1;

}