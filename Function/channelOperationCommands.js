const Channel = require('../Model/channel').Channel;
var errorMessages = require('../Model/errorMessages').erroMessages;

exports.join = function(args, client, global) {

    //Cheks if the channel name is empty.
    if (args.length < 2) {
        var erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);
        return;

    }

    if (args.length == 2) {

        if (args[1] == 0) {

            if (client.channels.length == 0) {
                client.socket.write("JOIN :You are not on any channel.");
                return;
            }

            var partCommadCall = "PART " + client.channels.join(",") + "\r\n";
            partCommadCall = partCommadCall.split(" ");

            part(partCommadCall, client, global);

            return;

        } else {

            var channelsNames = args[1].includes(',') ? args[1].split(",") : [args[1]];

            channelsNames.forEach(channelName => {
                channelName = channelName.toUpperCase();
                var channel = {
                    "name": channelName,
                    "key": null
                };

                analyzeJoinChannel(client, channel, global);
            });

        }

    } else if (args.length == 3) {

        var channelsNames = args[1].includes(',') ? args[1].split(",") : [args[1]];
        var channelKeys = args[2].includes(',') ? args[2].split(",") : [args[2]];
        var channelMap = []

        channelsNames.forEach(channelName => {
            channelName = channelName.toUpperCase();
            var channel = {
                "name": channelName,
                "key": null
            };

            channelMap.push(channel);
        });

        channelKeys.forEach((key, index) => {
            channelMap[index].key = key;
        });

        channelMap.forEach(channel => {
            analyzeJoinChannel(client, channel, global);
        });

    }

    return;
}

exports.part = function(args, client, global) {
    part(args, client, global);
}

exports.topic = function(args, client, global) {
    topic(args, client, global);

    return;
}

function topic(args, client, global) {
    var erro = null;

    if (args.length < 2) {
        erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);

        return;
    }

    var channelName = args[1].toUpperCase();
    var channel = findChannel(channelName, global);

    if (channel == null) {
        erro = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', args[1].toUpperCase());
        client.socket.write(erro);
        return;
    }

    if (!client.mode.includes('o') && !client.mode.includes('k') && (channel.operator != client.nick.toUpperCase())) {
        erro = erroMessages['ERR_CHANOPRIVSNEEDED'][1].replace('<channel>', channel.name);
        client.socket.write(erro);
        return;
    }

    if (!client.channels.includes(args[1].toUpperCase())) {
        client.socket.write("JOIN :You are not on any channel");
        return;
    }

    if (args.length == 2) {

        var topicMessage = channel.topic;

        if (topicMessage == null) {
            client.socket.write(channel.name + " :No topic is set\r\n");

            return;
        }

        client.socket.write(channel.name + " :" + topicMessage + "\r\n");

        return;

    }

    if (args.length > 2) {

        if (!args[2].charAt(0) == ":") {
            client.socket.write("TOPIC: Syntax error\r\n");

            return;
        }

        var topicMessage = args.join(" ");
        var returnMessage = "TOPIC :";

        if (topicMessage.charAt(topicMessage.length - 1) != ":") {
            topicMessage = topicMessage.split(":")[1];
            returnMessage = returnMessage + "Topic added\r\n";

        } else {

            topicMessage = null;
            returnMessage = returnMessage + "Topic cleaned\r\n";
        }

        channel.topic = topicMessage;

        updateChannel(channel, global);

        client.socket.write(returnMessage);

    }

    return;
}

exports.names = function(args, client, global) {
    var erro = null;

    if (args.length > 2) {
        client.socket.write("TOPIC :Syntax error\r\n");
        return;
    }

    var noOnePublicChannel = true;
    var message = "";

    if (args.length == 1) {
        if (!global.channels.length) {
            client.socket.write("NAMES :There are no registered channels\r\n");
            return;
        }

        global.channels.forEach(channel => {
            if (channel.mode == "=") {
                message = message + showChannelNames(channel, client, global, true);
                noOnePublicChannel = false;
            }
        });

        client.socket.write(message);

    }

    if (args.length == 2) {
        args[1] = args[1].toUpperCase();
        var channelsNames = args[1].includes(",") ? args[1].split(",") : [args[1]];
        var channel = null;

        channelsNames.forEach(channelName => {
            channel = findChannel(channelName, global);
            if (channel != null) {
                message = message + showChannelNames(channel, client, global, true);

                noOnePublicChannel = false;

                client.socket.write(message);

            } else {
                erro = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', channelName);
                client.socket.write(erro);
                noOnePublicChannel = false;
            }
        });
    }

    if (noOnePublicChannel)
        client.socket.write("NAMES :There are no registered public channels\r\n");

    return;
}

exports.list = function(args, client, global) {
    var erro = null;

    if (args.length == 1) {

        if (global.channels.length > 0) {

            global.channels.forEach(ch => {
                listChannel(ch, client, global);
            });

        } else
            client.socket.write("LIST :There are no registered channels\r\n");

        return;

    }

    args[1] = args[1].toUpperCase();

    if (args.length == 2) {
        var channelsNames = args[1].includes(",") ? args[1].split(",") : [args[1]];
        var channel = null;

        channelsNames.forEach(chn => {

            channel = findChannel(chn, global);

            if (channel != null)
                listChannel(channel, client, global);
            else {
                erro = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', chn);
                client.socket.write(erro);
            }

        });

    } else {
        client.socket.write("LIST :Syntax error\r\n");
    }

    return;
}

exports.invite = function(args, client, global) {
    var erro = null;

    if (args.length < 3) {
        erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);
        return;
    }

    args[2] = args[2].toUpperCase();
    var guest = findClient(args[1], global);
    var channel = findChannel(args[2], global);

    if (guest == null) {
        erro = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', args[1].toUpperCase());
        client.socket.write(erro);
        return;
    }

    if (client.channels.includes(args[2].toUpperCase()) || channel == null) {

        if (guest.channels.includes(args[2])) {
            erro = errorMessages['ERR_USERONCHANNEL'][1].replace('<user>', guest.nick);
            erro = erro.replace('<channel>', args[2]);
            client.socket.write(erro);

        } else {
            guest.socket.write(":" + client.nick + "!" + client.realName + "@" + global.serverName + " INVITE " + guest.nick + " " + args[2] + "\r\n")
            client.socket.write(args[2] + " " + guest.nick + "\r\n");
        }

    } else {
        erro = errorMessages['ERR_NOTONCHANNEL'][1].replace('<channel>', args[2]);
        client.socket.write(erro);
    }

    return;
}

exports.kick = function(args, client, global) {
    var erro = null;
    if (args.length < 3) {
        erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);
        return;
    }

    args[1] = args[1].toUpperCase();

    kickUser = args[2].toUpperCase();

    kickUser = findClient(kickUser, global);

    if (kickUser == null) {
        erro = errorMessages['ERR_NOSUCHNICK'][1].replace('<nickname>', args[2].toUpperCase());
        client.socket.write(erro);
        return;
    }

    var channelsNames = args[1].includes(',') ? args[1].split(',') : [args[1]];
    var messageModel = client.nick + " KICK #channelName " + args[2] + "\r\n";
    var channel = null;

    var costumerMessage = args.join(" ").split(":")[1];
    costumerMessage = costumerMessage == null || costumerMessage == undefined ? 'Banned from the channel' : costumerMessage;

    if (costumerMessage != "") {
        messageModel = messageModel.split(":")[0].replace("\r\n", " :") + costumerMessage + "\r\n";
    }

    var broadCastMessage = messageModel;

    channelsNames.forEach(channelName => {
        channel = findChannel(channelName, global);

        if (channel == null) {
            erro = errorMessages['ERR_NOSUCHCHANNEL'][1].replace('<channel>', channelName);
            client.socket.write(erro);
            return;
        }

        if (!client.mode.includes('o') && !client.mode.includes('k') && (channel.operator != client.nick.toUpperCase())) {
            erro = errorMessages['ERR_CHANOPRIVSNEEDED'][1].replace('<channel>', channelName);
            client.socket.write(erro);
            return;
        }

        if (kickUser.channels.includes(channelName) && channel != null) {

            outChannel(kickUser, channel, global);

            broadCastMessage = broadCastMessage.replace("#channelName", channelName);
            client.socket.write(broadCastMessage);

            kickUser.socket.write(broadCastMessage);

            broadcastMessageFunc(client.socket.name, broadCastMessage, global, channel.members);

            broadCastMessage = messageModel;

        } else if (channel == null) {
            erro = errorMessages['ERR_USERONCHANNEL'][1].replace('<channel>', channelName);
            client.socket.write(erro);

        } else {
            erro = errorMessages['ERR_USERNOTINCHANNEL'][1].replace('<nick>', kickUser.nick);
            erro = erro.replace('<channel', channel.name);
            client.socket.write(erro);
        }

    });

    return;
}

// Support Functions
function listChannel(channel, client, global) {

    showChannelInformation(channel, client, global);
    return;
}

function analyzeJoinChannel(client, channelJoin, global) {
    var erro = null;
    if (channelNameIsValid(channelJoin.name, client, global) == false)
        return;

    var channel = findChannel(channelJoin.name, global);

    //Checks if the channel already exists.
    //In positive case.    
    if (channel != null && (channel.key === null || channel.key === channelJoin.key)) {
        if (!channel.members.includes(client.nick)) {
            //Adds client to channel.
            addClient(client, channel, global, false);

        } else {
            client.socket.write(channelJoin.name + " :You are already in channel\r\n");

        }


    } else if (channel != null && channel.key != channelJoin.key) {
        erro = errorMessages['ERR_BADCHANNELKEY'][1].replace('<channel>', channelJoin.name);
        client.socket.write(erro);

        //In negative case.
    } else {

        //Creates a new channel.
        channel = new Channel(channelJoin.name);
        channel.key = channelJoin.key;

        //Adds client to channel.
        addClient(client, channel, global, true);
    }



    return;
}

function addClient(client, channel, global, newChannel) {

    var nick = client.nick;

    //Adds client to channel.
    channel.members.push(nick);
    client.channels.push(channel.name);

    client.nick = nick.toUpperCase();
    indexClient = global.clients.findIndex(c => c.socket.name === client.socket.name);
    global.clients.splice(indexClient, 1);
    global.clients.push(client);

    var messageBroadcast = channel.name + " :" + client.nick + " joined the channel\r\n";
    var newMemberMessage = channel.name + " :Welcome to channel\r\n";

    client.socket.write(newMemberMessage);

    if (newChannel) {
        channel.operator = client.nick.toUpperCase();
        //Adds the new channel to the server channel list.
        global.channels.push(channel);

    } else if (channel.members.length > 1) {
        showChannelInformation(channel, client, global);
    }

    //Sends message for all members
    broadcastMessageFunc(client.socket.name, messageBroadcast, global, channel.members);

}

function broadcastMessageFunc(from, message, global, membersNicks) {
    var clientMembers = global.clients.filter(client => membersNicks.includes(client.nick.toUpperCase()) && client.socket.name !== from);

    clientMembers.forEach(function(client) {
        client.socket.write(message);
    });

    return;
}

function part(args, client, global) {
    var erro = null;
    if (args.length < 2) {
        var erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);
        return;
    }

    args[1] = args[1].toUpperCase();
    var channelsNames = args[1].includes(',') ? args[1].split(',') : [args[1]];
    var messageModel = client.nick + " PART #channelName :I am leaving this channel\r\n";
    var costumerMessage = args.join(" ").split(":")[1];
    var channel = null;

    if (costumerMessage) {
        messageModel = messageModel.split(":")[0].replace(".\r\n", " :") + costumerMessage + ".\r\n";
    }

    var broadCastMessage = messageModel;

    channelsNames.forEach(channelName => {
        channel = findChannel(channelName, global);

        if (client.channels.includes(channelName) && channel != null) {

            broadCastMessage = broadCastMessage.replace("#channelName", channelName);

            client.socket.write("PART " + channelName + " :You left this channel\r\n");

            outChannel(client, channel, global);

            broadcastMessageFunc(client.socket.name, broadCastMessage, global, channel.members);

            broadCastMessage = messageModel;

        } else if (channel == null) {
            erro = errorMessages['ERR_NOSUCHCHANNEL'][1].replace('<channel>', channelName);
            client.socket.write(erro);

        } else {
            erro = errorMessages['ERR_NOTONCHANNEL'][1].replace('<channel>', channelName);
            client.socket.write(erro);
        }

    });

}

function showChannelNames(channel, client, global, onlyVisibleUsers) {
    var message = channel.mode + channel.name + " :";

    if (channel.members.length > 0) {
        channel.members.forEach(memberNick => {
            var userMode = findClient(memberNick, global).mode == "i" ? "@" : "+";

            if (onlyVisibleUsers == false || userMode != "i")
                message = message + userMode + memberNick + " ";
        });

    } else {
        message = message + "Não há membros nesse canal.";
    }

    message = message + "\r\n";

    return message;
}

function showChannelInformation(channel, client, global) {
    var topicMessage = channel.name;
    topicMessage = channel.topic == null ? topicMessage + " :Não possui TÓPICO cadastrado" : topicMessage + " :" + channel.topic;
    var message = topicMessage + "\r\n";
    client.socket.write(message);

    message = channel.mode + channel.name + " :";
    message = showChannelNames(channel, client, global, false);

    message = message + "\r\n";

    client.socket.write(message);
}

function updateChannel(channel, global) {

    indexChannel = global.channels.findIndex(c => channel.name === c.name);
    global.channels.splice(indexChannel, 1);
    global.channels.push(channel);

    return;
}

function updateClient(client, global) {

    indexClient = global.clients.findIndex(c => client.socket.name === c.socket.name);
    global.clients.splice(indexClient, 1);
    global.clients.push(client);

    return;
}

function findChannel(channelName, global) {

    var channel = global.channels.find(ch => ch.name.toUpperCase() == channelName.toUpperCase());
    return channel;
}

function outChannel(client, channel, global) {
    client.channels.splice(client.channels.indexOf(channel.name), 1);
    updateClient(client, global);

    channel.members.splice(channel.members.indexOf(client.nick), 1);
    updateChannel(channel, global);

    return;
}

function channelNameIsValid(channelName, client, global) {

    var validChannelName = true;

    if (channelName.includes(' ') || channelName.includes(',' || channelName.includes(':'))) {
        client.socket.write("JOIN :Spaces, commas, and colon are not allowed in channel names\r\n");
        validChannelName = false;
    }

    if (channelName.length > global.maximumChannelNameSize) {
        client.socket.write("JOIN :Maximum length is 50 characters for channel names\r\n");
        validChannelName = false;

    }

    if (!global.channelNamesPrefixes.includes(channelName.charAt(0))) {
        client.socket.write("JOIN :Channel names must start with one of the prefixes in colcheites [ " + global.channelNamesPrefixes.join(",") + " ]\r\n");
        validChannelName = false;
    }

    return validChannelName;
}

function findClient(nick, global) {
    nick = nick.toUpperCase();
    var client = global.clients.find(cl => cl.nick.toUpperCase() == nick);
    return client;
}