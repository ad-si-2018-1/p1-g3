const commandsMap = require('../Function/commandsMap').commandsMap;

exports.analyze = function(data, socket, global) {

    let message = String(data);

    var commands = message.split("\r\n");

    commands.forEach(function(commandMessage) {
        if (commandMessage.length)
            executeComand(commandMessage, socket, global);
    });

    return;
}

function validRegister(client) {

    var valid = true;

    if (client.nick == "" || client.nick == null) {
        client.socket.write("IRC-CHAT :Execute o comando NICK para iniciar seu registro.\r\n");
        valid = false;
    }

    if (client.realName == "" || client.realName == null) {
        client.socket.write("IRC-CHAT :Execute o comando USER para continuar seu registro.\r\n");
        valid = false;
    }

    return valid;
}

function executeComand(args, socket, global) {
    var nomeComando = args;
    args = args.trim().split(" ");
    var command = args[0].toUpperCase();
    command = commandsMap[command];

    var client = global.clients.find(cli => {
        if (cli.socket.name == socket.name)
            return cli;
    });

    var command = args[0].toUpperCase();
    var validRegisterClient = true;

    if (command != "NICK" && command != "USER") {
        validRegisterClient = validRegister(client);
    }

    if (command != "QUIT") {
        client.pingInformation.time = Date.now();
        client.pingInformation.status = 1;
    }

    //Checks register's client
    if (validRegisterClient == false)
        return;

    command = commandsMap[command];

    //Check if command exists.
    var commanValid = ((command !== null) && (command !== undefined));

    if (commanValid) {
        command(args, client, global);
        if (isNaN(global.counter[nomeComando])) {
            global.counter[nomeComando] = 1;
        } else {
            global.counter[nomeComando]++;
        }

    } else {
        commanValid = false;
        socket.write(args[0].toUpperCase() + " :Unknown command\n\r");
    }
}