var part = require('./channelOperationCommands').part;

exports.nick = function(args, client, global) { //FEITO

    var nick = args[1]

    //se o nick for vazio
    if (!nick){
        client.socket.write("NICK está vazio\r\n");
        return

    //procura o nick no array de nicks
    }else if (global.clients.find(c => c.nick == nick.toUpperCase()) != null) {
        client.socket.write("NICK: " + nick.toUpperCase() + "!" + " já existe\r\n");
        return
    } else if (nick.length > 9) {
        client.socket.write(": " + global.serverName + " " + (nick || "*") + " NICK : Quantidade maxima de caracteres = 9.\r\n");
        return
    } else {

        //compara se já existe um nick para o user, e em seguida se vdd deleta o nick antigo para atribuir o novo nick
        if (client.nick) {
            var nickCH = nick
            client.nick = "";

        }
        /* Informa a mudança de nick para todos os usuários
        for (cl in global.clients) {
            global.clients[cl].socket.write(":" + client.nick + "!" + client.realName + "@" + client.nick + " " + "NICK" + " " + ":" + args[1] + "\r\n");
          }
        */
        for (channel in client.channels) { // tratar o nick nos canais 
            for (mb in client.channels[channel].members) {
                if (mb == nickCH) {
                    client.channels[channel].members[mb] = nick
                }
            }
        }
        client.nick = nick.toUpperCase();
        indexClient = global.clients.findIndex(c => c.nick == client.nick);
        global.clients.splice(indexClient, 1);
        global.clients.push(client);

        client.socket.write("Comando NICK efetuado com sucesso\r\n");
        return
    }

}

exports.user = function(args, client, global) { //FEITO

    if (args.length < 4) {
        client.socket.write(": localhost " + (client.nick || "*") + " USER :Quantidade de parâmetros insuficiente.\r\n");
        return

    }

    if (args[2] != 2 && args[2] != 3 && args[2] != 8 && args[2] != 0) {

        client.socket.write(": localhost " + (client.nick || "*") + " USER :Modo inválido.\r\n");
        return;
    }

    if (!args.join(" ").includes(":")) {
        client.socket.write("USER :Syntax error\r\n");
        return;
    }

    var nick = args[1].toUpperCase();

    var user = global.clients.find(c => c.nick == nick);

    if (user == null) {
        client.socket.write(":" + global.serverName + (client.nick || "*") + " USER :Usuário não encontrado.\r\n");
        return;

    }

    if (user.realName != "" && user.realName != null) {
        client.socket.write(": localhost " + (client.nick || "*") + client.realName + " USER :Comando não autorizado, já resgistrado.\r\n");
        return;

    }

    if (user.nick != client.nick) {
        client.socket.write(":" + global.serverName + (client.nick || "*") + " USER :Comando não autorizado.\r\n");
        return;
    }

    switch (args[2]) {
        case "0":
            args[2] = "r";
            break;
        case "2":
            args[2] = "w";
            break;
        case "3":
            args[2] = "i";
            break;
        case "8":
            args[2] = "i";
            break;
    }

    
    /*
    indexClient = global.clients.findIndex(c => c.nick == client.nick);
    global.clients.splice(indexClient, 1);
    global.clients.push(client);
    */
    client.nick = nick.toUpperCase();
    client.realName = args.join(" ").split(":")[1].toUpperCase()
    client.mode.push(args[2])
    indexClient = global.clients.findIndex(c => c.nick == client.nick);
    global.clients.splice(indexClient, 1);
    global.clients.push(client);
    client.socket.write("USER: Comando efetuado\r\n");

    return;
}

exports.quit = function(args, client, global) { //FEITO
    if (client.channels.length > 0) {
        var partCommandCall = "PART " + client.channels.join(",");
        partCommandCall = partCommandCall.split(" ");
        part(partCommandCall, client, global);
    }

    kickoutUser(global, client)

    client.socket.write(":" + client.nick + "!" + client.realName + "@" + " " + "CLIENT QUIT\r\n") //msg do cliente saindo
    setTimeout(function() {
        client.socket.end()
    }, 3000);
    //funcão para retirar o cliente
    return
}

exports.squit = function(args, client, global) {
    client.socket.write("Comando SQUIT");
}

exports.oper = function(args, client, global) { //FEITO
    var nick = args[1].toUpperCase()
    var pass = args[2]
    var user = global.clients.find(c => c.nick == nick)

    if (user == null) {
        client.socket.write("Nick:" + nick + " " + "não encontrado\r\n")
        return
    } else if (user != null) {
        if (client.pass == "") {
            client.socket.write("Senha invalida ou em branco, favor realizar comando PASS\r\n")
            return
        } else if (user.pass == pass) {
            if (client.mode.find(m => m == "k") && client.mode.find(m => m == "c")) {
                client.socket.write("OPER: já registrado para determinado o USER\r\n")
                return
            } else {
                client.mode.push("k")
                client.mode.push("c")
                indexClient = global.clients.findIndex(c => c.nick === client.nick)
                global.clients.splice(indexClient, 1)
                global.clients.push(client)
                client.socket.write("OPER: Comando efetuado\r\n")
                return
            }
        } else {
            client.socket.write("PASS: invalido\r\n")
            return
        }
    }
}

exports.pass = function(args, client, global) { //FEITO
    var pass = args[1]
    if (!pass) {
        client.socket.write("PASS está vazio\r\n")
    } else {
        if (client.pass) {
            client.pass = ""
        }
        client.pass = pass
        indexClient = global.clients.findIndex(c => c.nick === client.nick);
        global.clients.splice(indexClient, 1);
        global.clients.push(client);
        client.socket.write("Comando PASS efetuado com sucesso\r\n");
    }

}

function kickoutUser(global, client) {

    indexClient = global.clients.findIndex(c => c.nick == client.nick);
    global.clients.splice(indexClient, 1);

    return;
}