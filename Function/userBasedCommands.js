var errorMessages = require('../Model/errorMessages').erroMessages;
// Verifica quais clientes estão no canal/sala passado
exports.who = function(args, client, global) {
    var erro = null;
    var operatorParam = args.length == 3 ? args[2].toUpperCase() : null;

    if (operatorParam != null && operatorParam != 'O') {
        erro = "WHO :" + args[2] + " is a no valid param\r\n";
        client.socket.write(erro);
        return;
    }


    if (args.length < 2) {
        erro = errorMessages['ERR_NEEDMOREPARAMS'][1].replace('<command>', args[0].toUpperCase());
        client.socket.write(erro);
        return;

    }

    var searchWho = args[1].toUpperCase();

    while (searchWho.includes('*')) {
        searchWho = searchWho.replace('*', '');
    }

    var channelMatch = false;

    global.channels.forEach(chn => {
        if (chn.name.includes(searchWho)) {
            channelMatch = whoForChannel(client, chn, operatorParam, searchWho, global) ? true : channelMatch;

        }
    });

    var userMacth = false;

    if (!channelMatch) {
        var count = 1;
        global.clients.forEach(user => {
            if ((user.nick.includes(searchWho) || user.realName.includes(searchWho)) && !user.mode.includes('i')) {
                count = whoForUser(client, user, operatorParam, count, global);
                userMacth = true;
            }
        });

    }

    if (!userMacth && !channelMatch) {
        client.socket.write("WHO :No users found\r\n");

    } else if (userMacth) {
        client.socket.write("End of WHO list\r\n");
    }

    return;
}

exports.whois = function(args, client, global) {

    if (!args[1]) {
        client.socket.write(": " + global.serveName + "461 " + client.nick + " WHOIS: parametros insuficientes\r\n")
        return
    }

    var user = global.clients.find(c => c.nick === args[1].toUpperCase())

    if (user == null) {
        client.socket.write(": " + global.serveName + "401 " + client.nick + " " + args[1] + "NICK/CHANNEL não encontrados\r\n")
        client.socket.write(": " + global.serveName + "318 " + client.nick + " " + args[1] + "Fim WHO list\r\n")
        return
    } else {
        var channelInCommon = []
        for (i = 0; i < global.channels.length; i++) //trata canais identicos
            if (client.channels[i] == user.channels[i]) {
            channelInCommon[i] = client.channels[i]
        }
    }
    client.socket.write("NICK: " + user.nick + " \r\n") //exibe o nick
    client.socket.write("REALNAME: " + user.realName + " \r\n") //exibe o nome real
    for (i = 0; i < user.channels.length; i++) client.socket.write("CHANNEL: " + user.channels[i] + " \r\n") //exibes os canais do qual o user faz parte
    client.socket.write("Canais em comum entre USER: " + client.nick + " e USER " + user.nick + " \r\n")
    for (i = 0; i < user.channels.length; i++) client.socket.write("CHANNEL IN COMMOM: " + channelInCommon[i] + " \r\n") //exibes os canais em comum entre os users

}

exports.whowas = function(args, client, global) {
    client.socket.write("Comando WHOWAS")
}

function whoForChannel(client, channel, operatorParam, searchWho, global) {

    var returnMessageModel = "<channel> <user> <host> localhost <nick> :<hopcount> <real name>\r\n";
    var returnMessage = returnMessageModel;
    var user = null;
    var count = 1;

    channel.members.forEach(function(nick) {

        user = global.clients.find(client => client.nick == nick);

        if (!user.mode.includes('i') && (operatorParam == null || user.mode.includes('o'))) {
            returnMessage = returnMessage.replace('<channel>', channel.name);
            returnMessage = returnMessage.replace('<host>', user.socket.remoteAddress);
            returnMessage = returnMessage.replace('<user>', user.socket.name);
            returnMessage = returnMessage.replace('<nick>', user.nick);
            returnMessage = returnMessage.replace('<real name>', user.realName);
            returnMessage = returnMessage.replace('<hopcount>', count);

            client.socket.write(returnMessage);
            returnMessage = returnMessageModel;
            count = count + 1;
        }

    });

    if (count == 1)
        client.socket.write("WHO :No users found\r\n");
    else
        client.socket.write("End of WHO list\r\n");

    return (count > 1);
}

function whoForUser(client, user, operatorParam, count, global) {

    var returnMessageModel = "<user> <host> localhost <nick> :<hopcount> <real name>\r\n";
    var returnMessage = returnMessageModel;

    if (!user.mode.includes('i') && (operatorParam == null || user.mode.includes('o'))) {
        returnMessage = returnMessage.replace('<host>', user.socket.remoteAddress);
        returnMessage = returnMessage.replace('<user>', user.socket.name);
        returnMessage = returnMessage.replace('<nick>', user.nick);
        returnMessage = returnMessage.replace('<real name>', user.realName);
        returnMessage = returnMessage.replace('<hopcount>', count);

        client.socket.write(returnMessage);
        returnMessage = returnMessageModel;
        count = count + 1;
    }

    return count;
}