const serverQueries = require('../Function/serverQueries');
const connectionRegisterCommands = require('../Function/connectionRegisterCommands');
const channelOperationCommands = require('../Function/channelOperationCommands');
const userBasedCommands = require('../Function/userBasedCommands');
const miscellaneousCommands = require('../Function/miscellaneousCommands');
const sendMessageCommands = require('../Function/sendMessageCommands');
var time = require('../Model/time').time;

exports.commandsMap = {

    //Server's Queries
    "MOTD": serverQueries.motd,
    "LUSERS": serverQueries.lusers,
    "VERSION": serverQueries.version,
    "STATS": serverQueries.stats,
    "TIME": serverQueries.time,

    //Connection Register Commands
    "PASS": connectionRegisterCommands.pass,
    "NICK": connectionRegisterCommands.nick,
    "USER": connectionRegisterCommands.user,
    "OPER": connectionRegisterCommands.oper,
    "QUIT": connectionRegisterCommands.quit,

    //Channel's Operation Commands
    "JOIN": channelOperationCommands.join,
    "KICK": channelOperationCommands.kick,
    "TOPIC": channelOperationCommands.topic,
    "LIST": channelOperationCommands.list,
    "INVITE": channelOperationCommands.invite,
    "NAMES": channelOperationCommands.names,
    "PART": channelOperationCommands.part,

    //User Based Commands
    "WHO": userBasedCommands.who,
    "WHOIS": userBasedCommands.whois,
    "WHOAS": userBasedCommands.whoas,

    //Send Message Commands
    "PRIVMSG": sendMessageCommands.privmsg,
    "NOTICE": sendMessageCommands.notice,


    //Miscellaneous Commands
    "PING": miscellaneousCommands.ping,
    "PONG": miscellaneousCommands.pong,
}