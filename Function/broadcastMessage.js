// Broadcast to others, excluding the sender
exports.broadcast = function(from, message, global) {

    // If there are clients remaining then broadcast message
    global.clients.forEach(function(client) {

        // Dont send any messages to the sender
        if (client.socket.name != from)
            client.socket.write(message);

    });

};