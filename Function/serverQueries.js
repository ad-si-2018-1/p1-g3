//Exibe a mensagem do dia do servidor
exports.motd = function(args, client, global) {
    client.socket.write(global.motd + '\r\n');
	client.socket.write('=======================================\r\n\n');
}

//Exibe as estatísticas sobre o tamanho do servidor
exports.lusers = function(args, client, global) {

    var QuantidadeCanais = global.channels.length;
    client.socket.write('Quantidade de Canais: ' + QuantidadeCanais + '\r\n');

    var QuantidadeClientes = global.clients.length;
    client.socket.write('Quantidade de Usuários: ' + QuantidadeClientes + '\r\n');
	client.socket.write('=======================================\r\n\n');
}

//Exibe a versão do server
exports.version = function(args, client, global) {
    client.socket.write('Versão do server: ' + global.version);
}

//Exibe dados do server
exports.stats = function(args, client, global) {
	
	if (!args[1]) {
		return client.socket.write("Server Grupo 3 Aplicações Distríbuidas \r\n");
	}
	else {
		
		switch (args[1]) {
			case 'l':
				client.socket.write('Este server é do tipo Single Server (tipo em que não exitem outros servidores conectados à esta rede)\r\n');
				client.socket.write('=======================================\r\n\n');
				break;
			case 'o':			
				client.socket.write('Dados dos Operadores: \r\n');
				var QuantidadeOperadores = 0
				if (global.clients.length == 0) {
					client.socket.write('Não existem usuários cadastrados \r\n');
					
				} else {								
					global.clients.forEach(function(c) {
						if (c.mode.includes('o')) {
							client.socket.write(c.nick + '\r\n' +
												c.realName + '\r\n' +
												c.socket.name + '\r\n' +
												'-----------------------\r\n');
							QuantidadeOperadores++;
						}
					});
					
					if (QuantidadeOperadores == 0) {
						client.socket.write('Não existem operadores cadastrados \r\n');
					}
				}
				client.socket.write('=======================================\r\n\n');
				break;
			case 'm':
				client.socket.write('Estatísticas de Utilização dos comandos: \r\n');
				client.socket.write('*Obs: o comando stats m executado agora não entra na contagem atual \r\n\n');
								
				for (var property in global.counter) {
					client.socket.write('Comando "' + property + '" executado ' + global.counter[property]);
					if (global.counter[property] == 1) {
						client.socket.write(' vez \r\n');
					} else {
						client.socket.write(' vezes \r\n');
					}
				}
				client.socket.write('=======================================\r\n\n');
				break;			
			case 'u':
				let tempoAtivo = Math.floor((Date.now() - global.started) / 1000);
				client.socket.write('Servidor ligado por: \r\n ' + Math.floor(tempoAtivo / 86400) + ' dias,\r\n ' + Math.floor((tempoAtivo % 86400) / 3600) + 					 ' horas,\r\n ' + Math.floor((tempoAtivo % 3600) / 60) + ' minutos e \r\n ' + tempoAtivo % 60 + ' segundos\r\n');
				client.socket.write('=======================================\r\n\n');
				break;
			default:
				client.socket.write('ERRO: comando desconhecido\r\n');
				client.socket.write('=======================================\r\n\n');
		}
	}
}


//Exibe o horário local do servidor
exports.time = function(args, client, global) {

    client.socket.write('Horário Local do Server: ' + global.hms() + '\n');
}

exports.connect = function(args, client, global) {

    client.socket.write('Comando CONNECT');
}

exports.info = function(args, client, global) {

    client.socket.write('Comando INFO');
}

exports.admin = function(args, client, global) {

    client.socket.write('Comando ADMIN');
}


  function boasVindas() {
    if (socket.nick && socket.user) {
      /* Replies para confirmar a conexão */
      socket.write(":" + serverName + " 001 " + socket.nick + " :Welcome to the Internet Relay Network " + socket.nick + "!" + socket.user.username + "@" + socket.name + "\r\r\n");
      socket.write(":" + serverName + " 002 " + socket.nick + " localhost\r\r\n");
      socket.write(":" + serverName + " 003 " + socket.nick + " :Server createde on " + createDate + "\r\r\n");
      socket.write(":" + serverName + " 004 " + socket.nick + " " + "localhost" + " " + "irc-server-1.1.1 " + "DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI" + "\r\r\n");
      server.getConnections(function(err, count) {
        socket.write(":" + serverName + " 251 " + socket.nick + " :There are " + count + " users\r\r\n");
        socket.write(":" + serverName + " 254 " + socket.nick + " " + Object.keys(channels).length + " :channels formed\r\r\n");
        motd();
      });
      if (clients.length > maxConnections) maxConnections = clients.length;
      connections++;
    };
  };
