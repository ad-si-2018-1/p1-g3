//Librarys
var net = require('net');
var quit = require('../Function/connectionRegisterCommands').quit;
var _ = require('../node_modules/lodash');

//Loads RFC's fucntions
var global = require('../Model/global').global;
const analyzeMessage = require('../Function/analyzeMessage').analyze;
//var broadcastMessage = require('../Function/broadcastMessage').broadcast;

//Load RFC's class
const Client = require('../Model/client').Client;

var server = net.createServer(function(socket) {

    // Identifica Esse Cliente
    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    var client = new Client(socket);

    global.clients.push(client);

    // Envia Uma Mensagem De Boas Vindas
    client.socket.write("Nós lhe desejamos as Boas Vindas " + client.socket.name + "\r\n");
    //broadcastMessage(client.socket.name, client.socket.name + " juntou-se ao chat\n", global);

    // Lida com as Mensagens Enviada dos clientes
    socket.on('data', function(data) {
        //broadcast(socket.name + "> " + data, socket);
        analyzeMessage(data, socket, global);
    });

    socket.on('error', function() {
        socket.destroy();
    });

}).listen(6667);

setInterval(checkClientConnection, 30000);

function checkClientConnection() {
    if (global.clients.length > 0) {
        var dif = 0;
        var quitCommandCall = ["QUIT"];

        global.clients.forEach(client => {
            dif = difDataInMin(client.pingInformation.time, Date.now());

            if (dif > global.pingInterval && client.pingInformation.status == 1) {
                client.socket.write(global.pingMessage.replace("?", client.nick));
                client.pingInformation.time = Date.now();
                client.pingInformation.status = 2;

            } else if (dif > global.pingInterval && client.pingInformation.status == 2) {
                client.pingInformation.time = Date.now();
                client.pingInformation.status = 3;
                quit(quitCommandCall, client, global);
            }
        });

    }


}

function difDataInMin(data1, data2) {
    var dif = data2 - data1;

    dif = Math.round(dif / 60000);
    return dif;
}

console.log("Server running on port 6667.\r\n");